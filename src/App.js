import * as React from 'react';

const postWithPromise = async function (apiUrl, body) {
  const res = await fetch(apiUrl, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
  });

  try {
      //e.g when 504, there is no json, will throw a error
      res.json = await res.json();
  } catch (e) {
      res.json = { failed: true }
  }

  return res;
};


const PaddingBetween = () => {
  return <div className='padding-between'></div>
}

export default function App() {
  const [chatGPTResults, setChatGPTResults] = React.useState([]);
  const [key2Images, setKey2Images]  = React.useState({});

  //----------------interaction
  const handleTextInput = async ()=> {
    const tempText =  document.querySelector(".input-textarea-for-prompt").value;
    // if(tempText.length == 0){
    //   return;
    // }
    // TODO send the to chatgpt
    // const res = await postWithPromise("http://localhost:3000/", {});
    // fake result
    setChatGPTResults([
      "1 Cras justo odio",
      "2 Dapibus ac facilisis in",
      "3 Porta ac consectetur ac",
      "4 Vestibulum at eros"
    ])
  }

  const handleGenerateImage = async () => {
    //// TODO send the to stable diffusion
    setKey2Images({
      "1 Cras justo odio": ["a1.jpg", "a2.jpg", "a3.jpg", "a4.jpg"],
      "2 Dapibus ac facilisis in": ["1.jpg", "2.jpg", "3.jpg", "4.jpg"],
      "3 Porta ac consectetur ac": ["1.jpg", "2.jpg", "3.jpg", "4.jpg"],
      "4 Vestibulum at eros": ["1.jpg", "2.jpg", "3.jpg", "4.jpg"],
    })
  }


  const handleRefresh = async(key, event) => {
    console.log(key);
  }

  //---------------------rendering
  const chatGPTResultDom = chatGPTResults.map(e => {
    return (<li className="list-group-item" key={e}>{e}</li>);
  })

  // when we have chatgpt result, time to do stable diffusion
  const hasSDButton = chatGPTResults.length == 4;

  const SDImagePanels = [];
  for(let kk in key2Images){
    const imgs = (key2Images[kk]||[]).map(e => {
      return (
        <div className="small-image-wrapper" key={e}>
          <img className='small-image-from-SD' src={e} alt="alternatetext" />
        </div>
      )
    })
    const temp = (<div className="container" key={kk}>
      <div className="row header-row-aa">
        <div className='small-title'>
          {kk}
        </div>
        <div>
          <button type="button" className="btn btn-dark" onClick={(e) =>handleRefresh(kk, e)}>Refresh</button>
        </div>
      </div>

      <div className="row">
        {imgs}
      </div>
    </div>);
    SDImagePanels.push(temp)
  } 

  return (
    <div className='app-page'>

      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="#">AI Example</a>
      </nav>

      <PaddingBetween/>
      <div className="input-group">
        <textarea className="form-control input-textarea-for-prompt" aria-label="With textarea"></textarea>
        <div className="input-group-prepend">
          <button type="button" className="btn btn-dark" onClick={handleTextInput}>確認</button>
        </div>
      </div>

      <PaddingBetween/>
      <ul className="list-group">
        {chatGPTResultDom}
      </ul>
      <PaddingBetween/>

      {
        hasSDButton && 
        (
        
        <div>
          <button type="button" className="btn btn-dark" onClick={handleGenerateImage}>画像後補を作成</button>
        </div>
        )
      }

      <PaddingBetween/>
      <div> 
      {SDImagePanels}
      </div>

    </div>
  );
}
